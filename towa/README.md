# Towa Testing task

## Installing

- Run a Symfony local web server through

```bash
symfony server:start
```

- Edit your *.env* file, indicating your local DB connection (engine, user, password, schema).

- Run a

```bash
php bin/console doctrine:schema:update --force
```

command, in scope of creating all the necessary DB tables.


## Importing User Data

- Go to [http://localhost:8001/users/import ](http://localhost:8001/users/import )page. Users data will be uploaded automatically.

## Retrieving User Data

- Go through the link on the previous page, to the [http://localhost:8000/users/show ](http://localhost:8000/users/show )page. A table with the Users list with the main data will be shown.

- Click on a table's row. Above the table will appear the full information about the user, it's company, address, and geolocation.

## Project structure

Besides the Entities creation, the main code was written in UserController, and AjaxController. The Twig templates are located in *templates/users* folder.

## License
[MIT](https://choosealicense.com/licenses/mit/)