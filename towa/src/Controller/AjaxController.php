<?php

namespace App\Controller;

use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ajax", name="ajax.")
 */
class AjaxController extends AbstractController
{
    /**
     * @Route("/getuserinfo", name="user_info")
     */
    public function userInfo(Request $request, ManagerRegistry $doctrine)
    {
        $userId = $request->get('userId');
        $em = $doctrine->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['id' => $userId]);
        return $this->render('users/user.html.twig', ['user' => $user]);
    }
}
