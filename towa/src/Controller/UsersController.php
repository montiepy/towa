<?php

namespace App\Controller;

use App\Entity\Address;
use App\Entity\Company;
use App\Entity\Geolocation;
use App\Entity\User;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/users", name="users.")
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/import", name="import")
     */
    public function index(HttpClientInterface $client, ManagerRegistry $doctrine): Response
    {

        $response = $client->request(
            'GET',
            'https://jsonplaceholder.typicode.com/users'
        );

        $content = $response->toArray();

        $this->persistingAPIData($content, $doctrine);

        return $this->render('users/index.html.twig');
    }

    /**
     * @Route("/show", name="show")
     */
    public function showUsers(ManagerRegistry $doctrine)
    {
        $em = $doctrine->getManager();
        $users = $em->getRepository(User::class)->findAll();
        return $this->render('users/users.html.twig', [
            'users' => $users,
        ]);
    }

    /**
     * @param array $userData
     * @param ManagerRegistry $doctrine
     */
    public function persistingAPIData(array $userData, ManagerRegistry $doctrine)
    {
        if (!empty($userData)) {
            foreach ($userData as $item) {

                $companyItem = $item['company'];
                $company = new Company();
                $company->setName($companyItem['name']);
                $company->setCatchPhrase($companyItem['catchPhrase']);
                $company->setBs($companyItem['bs']);

                $addressItem = $item['address'];
                $geolocationItem = $addressItem['geo'];

                $geolocation = new Geolocation();
                $geolocation->setLat($geolocationItem['lat']);
                $geolocation->setLng($geolocationItem['lng']);
                $doctrine->getManager()->persist($geolocation);

                $address = new Address();
                $address->setStreet($addressItem['street']);
                $address->setSuite($addressItem['suite']);
                $address->setCity($addressItem['city']);
                $address->setZipcode($addressItem['zipcode']);
                $address->setGeolocation($geolocation);
                $doctrine->getManager()->persist($address);


                $user = new User();
                $user->setName($item['name']);
                $user->setUsername($item['username']);
                $user->setEmail($item['email']);
                $user->setPhone($item['phone']);
                $user->setWebsite($item['website']);
                $user->setCompany($company);
                $user->setAddress($address);

                $doctrine->getManager()->persist($company);
                $doctrine->getManager()->persist($user);
            }
            $doctrine->getManager()->flush();

        }
    }
}
